<?php
/**
 * @package contextualcode/permissions-inheritance-bundle
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    20 July 2018
 * */

$FunctionList = array(
    'inheritances' => array(
        'name' => 'inheritances',
        'operation_types' => array('read'),
        'call_method' => array(
            'class' => 'ccPermissionsInheritanceFetchFunctions',
            'method' => 'fetchInheritances'
        ),
        'parameter_type' => 'standard',
        'parameters' => array(
            array(
                'name' => 'role_id',
                'type' => 'integer',
                'required' => true
            )
        )
    ),
    'roles_which_can_inherit' => array(
        'name' => 'roles_which_can_inherit',
        'operation_types' => array('read'),
        'call_method' => array(
            'class' => 'ccPermissionsInheritanceFetchFunctions',
            'method' => 'fetchRolesWhichCanInherit'
        ),
        'parameter_type' => 'standard',
        'parameters' => array(
            array(
                'name' => 'role_id',
                'type' => 'integer',
                'required' => true
            )
        )
    ),
    'parents' => array(
        'name' => 'inheritances',
        'operation_types' => array('read'),
        'call_method' => array(
            'class' => 'ccPermissionsInheritanceFetchFunctions',
            'method' => 'fetchParents'
        ),
        'parameter_type' => 'standard',
        'parameters' => array(
            array(
                'name' => 'role_id',
                'type' => 'integer',
                'required' => true
            )
        )
    ),
    'roles_which_can_be_parent' => array(
        'name' => 'roles_which_can_inherit',
        'operation_types' => array('read'),
        'call_method' => array(
            'class' => 'ccPermissionsInheritanceFetchFunctions',
            'method' => 'fetchRolesWhichCanBeParent'
        ),
        'parameter_type' => 'standard',
        'parameters' => array(
            array(
                'name' => 'role_id',
                'type' => 'integer',
                'required' => true
            )
        )
    ),
);
