<?php
/**
 * @package contextualcode/permissions-inheritance-bundle
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    20 July 2018
 * */

$Module = $Params['Module'];
$parentRoleID = (int) $Params['ParentRoleID'];
$inheritRoleID = (int) $Params['InheritRoleID'];

if ($parentRoleID <= 0 || $inheritRoleID <= 0) {
    return $Module->handleError(eZError::KERNEL_NOT_AVAILABLE, 'kernel');
}

$tmp = ccRoleInheritance::fetch($parentRoleID, $inheritRoleID);
if ($tmp instanceof ccRoleInheritance) {
    $tmp->remove();
}

eZCache::clearByID( 'user_info_cache' );

return $Module->redirectTo('/role/view/' . $parentRoleID);
