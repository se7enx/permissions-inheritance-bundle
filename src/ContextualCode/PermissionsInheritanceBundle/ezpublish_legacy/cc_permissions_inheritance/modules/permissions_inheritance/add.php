<?php
/**
 * @package contextualcode/permissions-inheritance-bundle
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    20 July 2018
 * */

$Module = $Params['Module'];
$parentRoleIDs = explode(',', $Params['ParentRoleID']);
$inheritRoleIDs = explode(',', $Params['InheritRoleID']);

if (count($parentRoleIDs) <= 0 || count($inheritRoleIDs) <= 0) {
    return $Module->handleError(eZError::KERNEL_NOT_AVAILABLE, 'kernel');
}

foreach ($parentRoleIDs as $parentRoleID) {
    foreach ($inheritRoleIDs as $inheritRoleID) {
        $tmp = ccRoleInheritance::fetch($parentRoleID, $inheritRoleID);
        if ($tmp instanceof ccRoleInheritance === false) {
            $roleInheritance = new ccRoleInheritance();
            $roleInheritance->setAttribute('parent_role_id', $parentRoleID);
            $roleInheritance->setAttribute('inherit_role_id', $inheritRoleID);
            $roleInheritance->store();
        }
    }
}

eZCache::clearByID( 'user_info_cache' );

return $Module->redirectTo('/role/view/' . $Params['RedirectRoleID']);
