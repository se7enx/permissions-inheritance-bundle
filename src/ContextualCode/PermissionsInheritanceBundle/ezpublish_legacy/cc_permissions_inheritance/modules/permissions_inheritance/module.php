<?php
/**
 * @package contextualcode/permissions-inheritance-bundle
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    20 July 2018
 * */

$Module = array(
    'name'      => 'Permissions Inheritance',
    'functions' => array()
);

$ViewList = array(
    'add' => array(
        'script'    => 'add.php',
        'functions' => array('manage'),
        'params'    => array('ParentRoleID', 'InheritRoleID', 'RedirectRoleID')
    ),
    'remove' => array(
        'script'    => 'remove.php',
        'functions' => array('manage'),
        'params'    => array('ParentRoleID', 'InheritRoleID', 'RedirectRoleID')
    )
);

$FunctionList = array(
    'manage' => array()
);
