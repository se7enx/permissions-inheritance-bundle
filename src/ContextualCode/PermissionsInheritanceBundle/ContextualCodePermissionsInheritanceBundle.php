<?php

namespace ContextualCode\PermissionsInheritanceBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use eZ\Bundle\EzPublishLegacyBundle\LegacyBundles\LegacyBundleInterface;

class ContextualCodePermissionsInheritanceBundle extends Bundle implements LegacyBundleInterface
{
    public function getLegacyExtensionsNames() {
        return array('cc_permissions_inheritance');
    }
}
